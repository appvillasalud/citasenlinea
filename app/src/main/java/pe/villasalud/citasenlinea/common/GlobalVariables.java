package pe.villasalud.citasenlinea.common;

public class GlobalVariables {
    private static String idsedeempresaadmin;
    public static String getIdsedeempresaadmin() {return idsedeempresaadmin;    }
    public static void setIdsedeempresaadmin(String idsedeempresaadmin) {
        GlobalVariables.idsedeempresaadmin = idsedeempresaadmin;
    }

    private static int spEmpresaPos;
    public static int getSpEmpresaPos() {return spEmpresaPos;    }
    public static void setSpEmpresaPos(int spEmpresaPos) {
        GlobalVariables.spEmpresaPos = spEmpresaPos;
    }

    private static String sede;
    public static String getSede() {
        return sede;
    }
    public static void setSede(String sede) {
        GlobalVariables.sede = sede;
    }

    private static int activity;
    public static int getActivity() {
        return activity;
    }
    public static void setActivity(int activity) {
        GlobalVariables.activity = activity;
    }
}