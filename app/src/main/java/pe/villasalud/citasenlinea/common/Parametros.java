package pe.villasalud.citasenlinea.common;

public class Parametros {

    /**
     * enlaces al entorno de desarrollo
     */
    //public static String URL_BASE = "http://168.121.223.121"; //ip externa
    public static String URL_BASE_LOGIN = "http://192.168.1.191:4000"; //ip local
    public static String URL_BASE_CONSULTA = "http://192.168.1.191:3010"; //ip local
    public static String URLCE = URL_BASE_LOGIN + "/api/";
    public static String URLCC = URL_BASE_CONSULTA + "/api/";

    /**
      enlaces al entorno de producción
     */
    /*public static String URL_BASE = "https://descuento.azurewebsites.net";
    public static String URLCE = URL_BASE + "/api/";*/
}
