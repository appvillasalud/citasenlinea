package pe.villasalud.citasenlinea.common;

import android.app.Activity;
import android.view.View;
import android.view.WindowManager;

public class Screen {
    public static void splashOE(Activity myActivityReference) {
        View decorView = myActivityReference.getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION //Oculta barra donde sale la hora, bateria, alertas de app, etc.
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION //Oculta barra inferior con los botones de control
                        | View.SYSTEM_UI_FLAG_FULLSCREEN //Oculta la barra de titulo de la app
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );
        myActivityReference.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public static void loginOE(Activity myActivityReference) {
        View decorView = myActivityReference.getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                //View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                //| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION //Oculta barra donde sale la hora, bateria, alertas de app, etc.
                //|
                View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                //| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION //Oculta barra inferior con los botones de control
                //| View.SYSTEM_UI_FLAG_FULLSCREEN //Oculta la barra de titulo de la app
                //| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );
        myActivityReference.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public static void appOE(Activity myActivityReference) {
        View decorView = myActivityReference.getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                //| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION //Oculta barra donde sale la hora, bateria, alertas de app, etc.
                //|
                //View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                //| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION //Oculta barra inferior con los botones de control
                //| View.SYSTEM_UI_FLAG_FULLSCREEN //Oculta la barra de titulo de la app
                //| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );
        //myActivityReference.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public static void ocultarEstado(Activity myActivityReference) {
        View decorView = myActivityReference.getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION //Oculta barra donde sale la hora, bateria, alertas de app, etc.
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION //Oculta barra inferior con los botones de control
                        | View.SYSTEM_UI_FLAG_FULLSCREEN //Oculta la barra de titulo de la app
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );
        myActivityReference.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}
