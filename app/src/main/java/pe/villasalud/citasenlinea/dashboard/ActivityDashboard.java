package pe.villasalud.citasenlinea.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;

import com.android.volley.RequestQueue;

import pe.villasalud.citasenlinea.R;
import pe.villasalud.citasenlinea.common.Screen;
import pe.villasalud.citasenlinea.dialog.DialogPerfilClinico;
//import pe.villasalud.citasenlinea.pacientes.ActivityPacientes;

public class ActivityDashboard extends AppCompatActivity implements View.OnClickListener{
    //Componentes
    private ConstraintLayout btn_perfil, perfilClinico, btn_consultas, btn_citas, btn_domicilio, btn_sedes, btn_configuracion, btn_soporte, btn_carrito;
    private EditText et_username, et_password;
    private RequestQueue requestQueue;
    private ProgressBar progressBar;
    private Button btn_pcCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Screen.loginOE(this);
        setContentView(R.layout.activity_dashboard);

        btn_perfil = findViewById(R.id.btn_perfil);
        btn_carrito = findViewById(R.id.btn_carrito);
        perfilClinico = findViewById(R.id.perfilClinico);
        btn_consultas = findViewById(R.id.btn_consultas);
        btn_citas = findViewById(R.id.btn_citas);
        btn_domicilio = findViewById(R.id.btn_domicilio);
        btn_sedes = findViewById(R.id.btn_sedes);
        btn_configuracion = findViewById(R.id.btn_configuracion);
        btn_soporte = findViewById(R.id.btn_soporte);

        //Declaración OnClick de los Botones
        btn_perfil.setOnClickListener(this);
        btn_carrito.setOnClickListener(this);
        btn_consultas.setOnClickListener(this);
        btn_citas.setOnClickListener(this);
        btn_domicilio.setOnClickListener(this);
        btn_sedes.setOnClickListener(this);
        btn_configuracion.setOnClickListener(this);
        btn_soporte.setOnClickListener(this);
    }

    private void mostrarBarraProgreso(){
        if(progressBar.getVisibility() == View.GONE){
            progressBar.setVisibility(View.VISIBLE);
        }else{
            progressBar.setVisibility(View.GONE);
        }
    }

    //Muestra Perfil Clinico
    public void mostrarPerfilClinico() {
        if(perfilClinico.getVisibility() == View.GONE){
            perfilClinico.setVisibility(View.VISIBLE);
        }else{
            perfilClinico.setVisibility(View.GONE);
        }
    }

    //Dialog para Cambiar Contraseña
    public void perfilClinico() {
        DialogFragment newFragment = new DialogPerfilClinico();
        newFragment.show(getSupportFragmentManager(), "dialogPerfilClinico");
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btn_carrito:
                break;
            case R.id.btn_perfil:
                perfilClinico();
                break;
            case R.id.btn_consultas:
                intent = new Intent(ActivityDashboard.this, ActivitySubmenu.class);
                intent.putExtra("opcion",1);
                startActivity(intent);
                break;
            case R.id.btn_citas:
                intent = new Intent(ActivityDashboard.this, ActivitySubmenu.class);
                intent.putExtra("opcion",2);
                startActivity(intent);
                break;
            case R.id.btn_domicilio:
                intent = new Intent(ActivityDashboard.this, ActivitySubmenu.class);
                intent.putExtra("opcion",3);
                startActivity(intent);
                break;
            case R.id.btn_sedes:
                intent = new Intent(ActivityDashboard.this, ActivitySubmenu.class);
                intent.putExtra("opcion",4);
                startActivity(intent);
                break;
            case R.id.btn_configuracion:
                intent = new Intent(ActivityDashboard.this, ActivitySubmenu.class);
                intent.putExtra("opcion",5);
                startActivity(intent);
                break;
            case R.id.btn_soporte:
                intent = new Intent(ActivityDashboard.this, ActivitySubmenu.class);
                intent.putExtra("opcion",6);
                startActivity(intent);
                break;
        }
    }
}