package pe.villasalud.citasenlinea.dashboard;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.android.volley.RequestQueue;

import pe.villasalud.citasenlinea.R;
import pe.villasalud.citasenlinea.common.Screen;
//import pe.villasalud.citasenlinea.pacientes.ActivityPacientes;

public class ActivitySubmenu extends AppCompatActivity implements View.OnClickListener{
    //Componentes
    private ConstraintLayout btn_perfil, perfilClinico, btn_soporte;
    private EditText et_username, et_password;
    private RequestQueue requestQueue;
    private ProgressBar progressBar;
    private Button btn_pcCerrar, btn_consultas;
    private ImageView img_soporte, img_consulta, img_citas, img_domicilio, img_sedes, img_configuracion;
    private TextView tv_titulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Screen.loginOE(this);
        setContentView(R.layout.activity_submenu);

        img_soporte = findViewById(R.id.img_soporte);
        img_consulta = findViewById(R.id.img_consulta);
        img_citas = findViewById(R.id.img_citas);
        img_domicilio = findViewById(R.id.img_domicilio);
        img_sedes = findViewById(R.id.img_sedes);
        img_configuracion = findViewById(R.id.img_configuracion);
        tv_titulo = findViewById(R.id.tv_titulo);

        Bundle datos = this.getIntent().getExtras();
        ajustarSizeIntent(datos.getInt("opcion"));

        //Declaración OnClick de los Botones
        img_soporte.setOnClickListener(this);
        img_consulta.setOnClickListener(this);
        img_citas.setOnClickListener(this);
        img_domicilio.setOnClickListener(this);
        img_sedes.setOnClickListener(this);
        img_configuracion.setOnClickListener(this);
    }

    private void mostrarBarraProgreso(){
        if(progressBar.getVisibility() == View.GONE){
            progressBar.setVisibility(View.VISIBLE);
        }else{
            progressBar.setVisibility(View.GONE);
        }
    }

    //Muestra Perfil Clinico
    public void mostrarPerfilClinico() {
        if(perfilClinico.getVisibility() == View.GONE){
            perfilClinico.setVisibility(View.VISIBLE);
        }else{
            perfilClinico.setVisibility(View.GONE);
        }
    }

    public void restablecerSize() {
        int newHeight = 60; // New height in pixels
        int newWidth = 60; // New width in pixels
        ImageView iv = findViewById(R.id.img_consulta);
        iv.requestLayout();
        iv.getLayoutParams().height = newHeight;
        iv.getLayoutParams().width = newWidth;
        iv.setScaleType(ImageView.ScaleType.FIT_XY);

        iv = findViewById(R.id.img_citas);
        iv.requestLayout();
        iv.getLayoutParams().height = newHeight;
        iv.getLayoutParams().width = newWidth;
        iv.setScaleType(ImageView.ScaleType.FIT_XY);

        iv = findViewById(R.id.img_domicilio);
        iv.requestLayout();
        iv.getLayoutParams().height = newHeight;
        iv.getLayoutParams().width = newWidth;
        iv.setScaleType(ImageView.ScaleType.FIT_XY);

        iv = findViewById(R.id.img_sedes);
        iv.requestLayout();
        iv.getLayoutParams().height = newHeight;
        iv.getLayoutParams().width = newWidth;
        iv.setScaleType(ImageView.ScaleType.FIT_XY);

        iv = findViewById(R.id.img_configuracion);
        iv.requestLayout();
        iv.getLayoutParams().height = newHeight;
        iv.getLayoutParams().width = newWidth;
        iv.setScaleType(ImageView.ScaleType.FIT_XY);

        iv = findViewById(R.id.img_soporte);
        iv.requestLayout();
        iv.getLayoutParams().height = newHeight;
        iv.getLayoutParams().width = newWidth;
        iv.setScaleType(ImageView.ScaleType.FIT_XY);
    }

    public void ajustarSize(int id) {
        int newHeight = 90; // New height in pixels
        int newWidth = 90; // New width in pixels
        ImageView iv = findViewById(id);
        iv.requestLayout();
        iv.getLayoutParams().height = newHeight;
        iv.getLayoutParams().width = newWidth;
        iv.setScaleType(ImageView.ScaleType.FIT_XY);
        switch (id) {
            case R.id.img_consulta:
                tv_titulo.setText(R.string.reserva_cita);
                break;
            case R.id.img_citas:
                tv_titulo.setText(R.string.cita_programada);
                break;
            case R.id.img_domicilio:
                tv_titulo.setText(R.string.servicio_domicilio);
                break;
            case R.id.img_sedes:
                tv_titulo.setText(R.string.nuestras_sedes);
                break;
            case R.id.img_configuracion:
                tv_titulo.setText(R.string.configuracion);
                break;
            case R.id.img_soporte:
                tv_titulo.setText(R.string.soporte);
                break;
        }
    }
    public void ajustarSizeIntent(int id) {
        int newHeight = 90; // New height in pixels
        int newWidth = 90; // New width in pixels
        ImageView iv = findViewById(id);

        switch (id) {
            case 1:
                iv = findViewById(R.id.img_consulta);
                tv_titulo.setText(R.string.reserva_cita);
                break;
            case 2:
                iv = findViewById(R.id.img_citas);
                tv_titulo.setText(R.string.cita_programada);
                break;
            case 3:
                iv = findViewById(R.id.img_domicilio);
                tv_titulo.setText(R.string.servicio_domicilio);
                break;
            case 4:
                iv = findViewById(R.id.img_sedes);
                tv_titulo.setText(R.string.nuestras_sedes);
                break;
            case 5:
                iv = findViewById(R.id.img_configuracion);
                tv_titulo.setText(R.string.configuracion);
                break;
            case 6:
                iv = findViewById(R.id.img_soporte);
                tv_titulo.setText(R.string.soporte);
                break;
        }
        iv.requestLayout();
        iv.getLayoutParams().height = newHeight;
        iv.getLayoutParams().width = newWidth;
        iv.setScaleType(ImageView.ScaleType.FIT_XY);
    }

    @Override
    public void onClick(View view) {
        restablecerSize();
        ajustarSize(view.getId());

        /*switch (view.getId()) {
            case R.id.img_consulta:
                tv_titulo.setText(R.string.reserva_cita);
                break;
            case R.id.img_citas:
                tv_titulo.setText(R.string.cita_programada);
                break;
            case R.id.img_domicilio:
                tv_titulo.setText(R.string.servicio_domicilio);
                break;
            case R.id.img_sedes:
                tv_titulo.setText(R.string.nuestras_sedes);
                break;
            case R.id.img_configuracion:
                tv_titulo.setText(R.string.configuracion);
                break;
            case R.id.img_soporte:
                tv_titulo.setText(R.string.soporte);
                break;
        }*/
    }
}