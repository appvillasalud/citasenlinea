package pe.villasalud.citasenlinea.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pe.villasalud.citasenlinea.R;
import pe.villasalud.citasenlinea.common.Parametros;

public class DialogCambiarPass extends DialogFragment{
    //Volley
    private RequestQueue requestQueue;
    private JsonObjectRequest jsonObjectRequest;

    private String accessToken;
    TextView btn_cancelar, btn_aceptar, et_pass1, et_pass2, et_pass3;
    Context context;

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog alertDialog;
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_cambiar_pass, null);
        final ProgressBar progressBarDialog;
        //final String passPrueba = "123456";

        btn_aceptar = v.findViewById(R.id.btn_aceptar);
        btn_cancelar = v.findViewById(R.id.btn_cancelar);
        et_pass1 = v.findViewById(R.id.et_pass1);
        et_pass2 = v.findViewById(R.id.et_pass2);
        et_pass3 = v.findViewById(R.id.et_pass3);
        progressBarDialog = v.findViewById(R.id.progressbarDialog);

        builder.setView(v);
        alertDialog = builder.create();
        // Add action buttons
        btn_aceptar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Aceptar
                        progressBarDialog.setVisibility(View.VISIBLE);
                            if(!et_pass2.getText().toString().equals("") && et_pass2.getText().length() >= 6){
                                progressBarDialog.setVisibility(View.GONE);
                                if(et_pass3.getText().toString().equals(et_pass2.getText().toString())){
                                    progressBarDialog.setVisibility(View.GONE);
                                    try {
                                        setUserData();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    JSONObject params = new JSONObject();
                                    try {
                                        params.put("oldPassword", et_pass1.getText().toString());
                                        params.put("newPassword", et_pass2.getText().toString());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    JsonObjectRequest JsonRequest = new JsonObjectRequest(Request.Method.POST, Parametros.URLCE + "password/change", params, new Response.Listener<JSONObject>() {
                                        @Override
                                        public void onResponse(JSONObject response) {
                                            Toast.makeText(getContext(), "Contraseña modificada satisfactoriamente", Toast.LENGTH_SHORT).show();
                                            alertDialog.dismiss();
                                        }
                                    }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            try{
                                                NetworkResponse response = error.networkResponse;
                                                if(response != null && response.data != null){
                                                    JSONObject jsonjObject = new JSONObject(new String(response.data));
                                                    String data = jsonjObject.getString("message");
                                                    Log.e("volleyPut","URL: "+data);
                                                    switch (response.statusCode){
                                                        case 401:
                                                            Toast.makeText(getContext(),"401 "+data,Toast.LENGTH_SHORT).show();
                                                            break;
                                                        default:
                                                            Toast.makeText(getContext(),"ERROR "+response.statusCode+". Comuniquese con el Area de Sistemas",Toast.LENGTH_SHORT).show();
                                                            break;
                                                    }
                                                }else{
                                                    Toast.makeText(getContext(),"Ocurrio un Error. Comuniquese con el Area de Sistemas",Toast.LENGTH_SHORT).show();
                                                }
                                            } catch (Throwable t) {
                                                t.printStackTrace();
                                            }
                                        }
                                    }){
                                        @Override
                                        public Map<String, String> getHeaders() throws AuthFailureError {
                                            Map<String,String> headers = new HashMap<>();
                                            headers.put("Authorization", "Bearer "+ accessToken);
                                            return  headers;
                                        }
                                    };
                                    requestQueue = Volley.newRequestQueue(getContext());
                                    requestQueue.add(JsonRequest);
                                }else{
                                    progressBarDialog.setVisibility(View.GONE);
                                    //Log.e("PreciosActivity","La Contraseña NO es Identica");
                                    Toast.makeText(getContext(),"Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                progressBarDialog.setVisibility(View.GONE);
                                //Log.e("PreciosActivity","La Contraseña NO es Diferente de Cero y/o NO es mayor a 6 caracteres");
                                Toast.makeText(getContext(),"La contraseña debe contener minimo 6 caracteres", Toast.LENGTH_SHORT).show();
                            }
                    }
                });
        btn_cancelar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                }
        );
        return alertDialog;
    }

    //SharedPreference con el TOKEN que llega desde LoginActivity
    private void setUserData() throws JSONException {
        SharedPreferences userdata = getActivity().getSharedPreferences("userdata", Context.MODE_PRIVATE);
        String sessiondata = userdata.getString("userdata","userdata");
        JSONObject objuserdata  = new  JSONObject(sessiondata);
        JSONObject jsonUserdata = new JSONObject(objuserdata.toString());
        accessToken = jsonUserdata.getString("accessToken");
    }
}