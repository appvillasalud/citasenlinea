package pe.villasalud.citasenlinea.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;

import pe.villasalud.citasenlinea.R;
import pe.villasalud.citasenlinea.login.ActivityLogin;

public class DialogCerrarSesion extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog alertDialog;
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_cerrar_sesion, null);
        TextView btn_cancelar, btn_aceptar;
        btn_aceptar = v.findViewById(R.id.btn_aceptar);
        btn_cancelar = v.findViewById(R.id.btn_cancelar);
        builder.setView(v);
        alertDialog = builder.create();
        // Add action buttons
        btn_aceptar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Aceptar
                        alertDialog.dismiss();
                        SharedPreferences userdata = getActivity().getSharedPreferences("userdata", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = userdata.edit();
                        editor.remove("userdata");
                        editor.clear();
                        editor.apply();
                        getActivity().finish();
                        Intent intent = new Intent(getActivity(), ActivityLogin.class);
                        startActivity(intent);
                    }
                }
        );
        btn_cancelar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                }
        );
        return alertDialog;
    }


}