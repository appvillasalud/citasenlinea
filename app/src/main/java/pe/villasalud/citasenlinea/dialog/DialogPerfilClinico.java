package pe.villasalud.citasenlinea.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pe.villasalud.citasenlinea.R;
import pe.villasalud.citasenlinea.common.Parametros;

public class DialogPerfilClinico extends DialogFragment{
    //Volley
    private RequestQueue requestQueue;
    private JsonObjectRequest jsonObjectRequest;

    private String accessToken;
    Button btn_pcCerrar;
    TextView btn_cancelar, btn_aceptar, et_pass1, et_pass2, et_pass3;
    Context context;

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog alertDialog;
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_perfil_clinico, null);
        //final String passPrueba = "123456";

        btn_pcCerrar = v.findViewById(R.id.btn_pcCerrar);
        /*btn_cancelar = v.findViewById(R.id.btn_cancelar);
        et_pass1 = v.findViewById(R.id.et_pass1);
        et_pass2 = v.findViewById(R.id.et_pass2);
        et_pass3 = v.findViewById(R.id.et_pass3);
        progressBarDialog = v.findViewById(R.id.progressbarDialog);*/

        builder.setView(v);
        alertDialog = builder.create();
        // Add action buttons

        btn_pcCerrar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                }
        );
        return alertDialog;
    }


}