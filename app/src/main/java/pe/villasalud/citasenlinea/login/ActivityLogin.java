package pe.villasalud.citasenlinea.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pe.villasalud.citasenlinea.R;
import pe.villasalud.citasenlinea.common.GlobalVariables;
import pe.villasalud.citasenlinea.common.Parametros;
import pe.villasalud.citasenlinea.common.Screen;
import pe.villasalud.citasenlinea.dashboard.ActivityDashboard;
//import pe.villasalud.citasenlinea.pacientes.ActivityPacientes;

public class ActivityLogin extends AppCompatActivity implements View.OnClickListener {
    //Componentes
    private Button btn_ingresar;
    private TextView btn_recpass, btn_registro;
    private EditText et_username, et_password;
    private RequestQueue requestQueue;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Screen.loginOE(this);
        setContentView(R.layout.activity_login);

        //Declaracion de Componentes
        btn_ingresar = findViewById(R.id.btn_ingresar);
        btn_recpass = findViewById(R.id.btn_recpass);
        btn_registro = findViewById(R.id.btn_registro);
        et_username = findViewById(R.id.et_username);
        et_password = findViewById(R.id.et_password);
        progressBar = findViewById(R.id.progressbar);

        //Declaración OnClick de los Botones
        btn_ingresar.setOnClickListener(this);//Declaración OnClick de los Botones
        btn_ingresar.setOnClickListener(this);
        btn_recpass.setOnClickListener(this);
        btn_registro.setOnClickListener(this);

        //Limpia el contenido del EditText Username al hacerle click
        et_username.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if (hasFocus) et_username.setText("");
            }
        });

        //Limpia el contenido del EditText Password al hacerle click
        et_password.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if (hasFocus) et_password.setText("");
            }
        });
    }

    private void mostrarBarraProgreso(){
        if(progressBar.getVisibility() == View.GONE){
            progressBar.setVisibility(View.VISIBLE);
        }else{
            progressBar.setVisibility(View.GONE);
        }
    }

    private void inicioSession(){
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Parametros.URLCE + "login", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonjObject = new JSONObject(response);
                    String data = jsonjObject.getString("data");

                    //Pasando "data" al Siguiente Activity
                    SharedPreferences userdata = getSharedPreferences("userdata", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = userdata.edit();
                    editor.putString("userdata",data);
                    editor.apply();

                    //Seteando la empresa por defecto
                    if(GlobalVariables.getIdsedeempresaadmin() == null){
                        GlobalVariables.setIdsedeempresaadmin("8");
                        GlobalVariables.setSede("VILLA EL SALVADOR");
                    }

                    //Pasando al Siguiente Activity
                    finish();
                    Intent intent = new Intent(ActivityLogin.this, ActivityDashboard.class);
                    startActivity(intent);
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Oculta la ProgressBar
                mostrarBarraProgreso();
                btn_ingresar.setEnabled(true);
                btn_ingresar.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.btn_rojo_sin_borde));

                try{
                    NetworkResponse response = error.networkResponse;
                    if(response != null && response.data != null){
                        JSONObject jsonjObject = new JSONObject(new String(response.data));
                        String data = jsonjObject.getString("message");
                        switch (response.statusCode){
                            case 401:
                                Toast.makeText(getApplicationContext(),data,Toast.LENGTH_SHORT).show();
                                break;
                            default:
                                Toast.makeText(getApplicationContext(),"ERROR "+response.statusCode+". Comuniquese con el Area de Sistemas",Toast.LENGTH_SHORT).show();
                                break;
                        }
                         }else{
                        Toast.makeText(getApplicationContext(),"Ocurrio un Error. Comuniquese con el Area de Sistemas",Toast.LENGTH_SHORT).show();
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> parametros = new HashMap<>();
                parametros.put("username",et_username.getText().toString());
                parametros.put("password",et_password.getText().toString());
                return parametros;
            }
        };
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Screen.loginOE((Activity) this);
    }

    //Deshabilita el Boton Atras
    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    //Oculta Teclado
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        View focusedView = getCurrentFocus();
        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        et_username.clearFocus();
        et_password.clearFocus();
        return true;
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btn_ingresar:
                // Muestra la ProgressBar
                mostrarBarraProgreso();
                btn_ingresar.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.btn_azul_sin_borde));
                //btn_ingresar.setTextColor(Color.parseColor("#FFFFFF"));
                btn_ingresar.setEnabled(false);
                //inicioSession();

                intent = new Intent(ActivityLogin.this, ActivityDashboard.class);
                startActivity(intent);
                break;
                case R.id.btn_recpass:
                    intent = new Intent(ActivityLogin.this, ActivityRecPass.class);
                    startActivity(intent);
                break;
                case R.id.btn_registro:
                    intent = new Intent(ActivityLogin.this, ActivityRegistro.class);
                    startActivity(intent);
                break;
        }
    }
}