package pe.villasalud.citasenlinea.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import pe.villasalud.citasenlinea.R;
import pe.villasalud.citasenlinea.common.GlobalVariables;
import pe.villasalud.citasenlinea.common.Parametros;
import pe.villasalud.citasenlinea.common.Screen;
//import pe.villasalud.citasenlinea.pacientes.ActivityPacientes;

public class ActivityRecPass extends AppCompatActivity {
    //Componentes
    private Button btn_recpass;
    private EditText et_username, et_password;
    private RequestQueue requestQueue;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Screen.loginOE(this);
        setContentView(R.layout.activity_recpass);

        btn_recpass = findViewById(R.id.btn_recpass);

        btn_recpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityRecPass.this, ActivityLogin.class);
                startActivity(intent);
            }
        });

    }

    private void mostrarBarraProgreso(){
        if(progressBar.getVisibility() == View.GONE){
            progressBar.setVisibility(View.VISIBLE);
        }else{
            progressBar.setVisibility(View.GONE);
        }
    }
}