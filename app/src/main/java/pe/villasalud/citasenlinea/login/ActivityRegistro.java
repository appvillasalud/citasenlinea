package pe.villasalud.citasenlinea.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.RequestQueue;

import pe.villasalud.citasenlinea.R;
import pe.villasalud.citasenlinea.common.Screen;
//import pe.villasalud.citasenlinea.pacientes.ActivityPacientes;

public class ActivityRegistro extends AppCompatActivity {
    //Componentes
    private Button btn_recpass;
    private EditText et_username, et_password;
    private RequestQueue requestQueue;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Screen.loginOE(this);
        setContentView(R.layout.activity_registro);

        btn_recpass = findViewById(R.id.btn_recpass);

        btn_recpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityRegistro.this, ActivityLogin.class);
                startActivity(intent);
            }
        });

    }

    private void mostrarBarraProgreso(){
        if(progressBar.getVisibility() == View.GONE){
            progressBar.setVisibility(View.VISIBLE);
        }else{
            progressBar.setVisibility(View.GONE);
        }
    }
}