package pe.villasalud.citasenlinea.splash;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import pe.villasalud.citasenlinea.R;
import pe.villasalud.citasenlinea.common.Screen;
import pe.villasalud.citasenlinea.login.ActivityLogin;

public class ActivitySplash extends AppCompatActivity {
    ImageView imgFondoSplash;
    TextView textViewTituloSplash;
    boolean animacion, tokenizacion;
    Animation frombottom, fromtop;
    private static int splashTimeOut=3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Screen.splashOE((Activity) this);
        setContentView(R.layout.activity_splash);
        splash();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Screen.splashOE((Activity) this);
        splash();
    }

    private void splash() {
        animacion = false;
        tokenizacion = false;

        imgFondoSplash = findViewById(R.id.imgFondoSplash);
        textViewTituloSplash = findViewById(R.id.textViewTituloSplash);

        frombottom = AnimationUtils.loadAnimation(this,R.anim.frombottom);
        fromtop = AnimationUtils.loadAnimation(this,R.anim.fromtop);

        imgFondoSplash.startAnimation(fromtop);
        textViewTituloSplash.startAnimation(frombottom);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                animacion = true;
                siguientePantalla();
            }
        },splashTimeOut);
    }

    private void siguientePantalla() {
        finish();
        Intent intent = new Intent(ActivitySplash.this, ActivityLogin.class);
        startActivity(intent);
    }
}